# NFT Marketplace

This repository contains the backend components of the NFT Marketplace application. The application allows users to view and filter a list of NFTs fetched from a GraphQL API. The backend is built using Node.js, Express.js, and TypeScript.

### Prerequisites

- Node.js
- npm (Node Package Manager)

### Installation

1. Clone the repository:

   `git clone https://gitlab.com/karenavdalyan/nft-app-server`

2. Change to the backend directory:

   `cd nft-app-server`

3. Install dependencies:

   `npm install`

### Configuration

Before running the backend, configure environment variables. Create a `.env` file in the root directory of the backend and add:

   ```
   PORT=<your-port>
   ```

   Replace `<your-port>` with the desired port for the server.

### Running the Application

Start the backend server:

   `npm start`

The server will be available at [http://localhost:your-port](http://localhost:your-port), where `<your-port>` is the port specified in the `.env` file.

### API Documentation

The backend provides a GraphQL API. Access the GraphQL playground at [http://localhost:your-port/graphql](http://localhost:your-port/graphql) in your browser to explore available queries and mutations.

### Testing

### Deployment

Deploy the backend following the deployment instructions for your chosen hosting platform. Ensure that you set the necessary environment variables for production.
