import { IResolvers } from "@graphql-tools/utils";
import { categoriesData } from "../data/categories.json";

const CategoryQuery: IResolvers = {
  Query: {
    categories: () => {
      return categoriesData;
    },
  },
};

export default CategoryQuery;
