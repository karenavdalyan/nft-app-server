import { merge } from "lodash";
import productResolvers from "./product";
import categoryResolvers from "./category";

const resolvers = merge(productResolvers, categoryResolvers);

export default resolvers;
