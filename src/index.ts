import express from "express";
import { ApolloServer } from "apollo-server-express";
import cors from "cors";
import schema from "./schema";
import resolvers from "./resolvers";
import { productsData } from "./data/productsData.json";

const app = express();
const port = process.env.PORT || 3001;

app.use(cors());

const server = new ApolloServer({
  typeDefs: schema,
  resolvers,
  context: { dataSources: { productsData } },
});

server.start().then(() => server.applyMiddleware({ app }));

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
