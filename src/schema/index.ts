import { makeExecutableSchema } from "graphql-tools";
import { merge } from "lodash";
import { GraphQLSchema } from "graphql";
import ProductType from "./productTypes";
import CategoryType from "./categoryTypes";
import ProductQuery from "./product";
import CategoryQuery from "./category";

const Query = `
  type Query {
    _empty: String
  }
`;

const SchemaDefinition = `
  schema {
    query: Query
  }
`;

const resolvers = {};

const schema: GraphQLSchema = makeExecutableSchema({
  typeDefs: [SchemaDefinition, Query, ProductType, CategoryType],
  resolvers: merge(resolvers, ProductQuery, CategoryQuery),
});

export default schema;
