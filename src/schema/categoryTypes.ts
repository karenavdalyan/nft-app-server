import { gql } from "apollo-server-express";

export const CategoryType = gql`
  type Category {
    name: String!
  }

  type Query {
    categories: [Category!]!
  }
`;

export default CategoryType;
