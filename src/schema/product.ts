import { IResolvers } from "@graphql-tools/utils";
import { productsData } from "../data/productsData.json";

const ProductQuery: IResolvers = {
  Query: {
    products: (_, { category, page, itemsPerPage }) => {
      let filteredProducts = productsData;

      if (category) {
        filteredProducts = filteredProducts.filter(
          (product) => product.category === category
        );
      }

      const totalItems = filteredProducts.length;
      const totalPages = Math.ceil(totalItems / itemsPerPage);

      const startIndex = (page - 1) * itemsPerPage;
      const endIndex = startIndex + itemsPerPage;

      const paginatedProducts = filteredProducts.slice(startIndex, endIndex);

      return {
        edges: paginatedProducts.map((product) => ({ node: product })),
        pageInfo: {
          currentPage: page,
          totalPages,
        },
      };
    },
    product: (_, { id }) => {
      return productsData.find((product) => product.id === id);
    },
  },
};

export default ProductQuery;
