import { gql } from "apollo-server-express";

export const ProductType = gql`
  type Product {
    id: ID!
    name: String!
    price: Float!
    category: String!
    image: String!
    description: String
  }

  type PageInfo {
    currentPage: Int!
    totalPages: Int!
  }

  type ProductConnection {
    edges: [Product!]!
    pageInfo: PageInfo!
  }

  type Query {
    products(category: String, page: Int, itemsPerPage: Int): ProductConnection!
    product(id: ID!): Product
  }
`;

export default ProductType;
